function padZero(num) {
    return num.toString().padStart(2, '0');
}

function updateTime() {
    const now = new Date();
    const hours = now.getHours();
    const minutes = padZero(now.getMinutes());
    const seconds = padZero(now.getSeconds());

    const timeElement = document.getElementById('time');
    if (timeElement) {
        timeElement.innerHTML = `${hours}:${minutes}:${seconds}`;
    } else {
        console.error('Element with ID "time" not found!');
    }
}

// 初始调用
updateTime();
// 设置定时器每秒调用一次
setInterval(updateTime, 1000);

    
var xhr = new XMLHttpRequest();
xhr.open('GET', './js/txt.js', true);
xhr.onreadystatechange = function() {
  if (xhr.readyState === 4 && xhr.status === 200) {
    var quotes = xhr.responseText.split('\n');
    var randomQuote = quotes[Math.floor(Math.random() * quotes.length)];
    document.getElementById('quote').innerHTML = randomQuote;
  }
};
xhr.send();

